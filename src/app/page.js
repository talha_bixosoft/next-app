import HomePage from "./home/page";
import ContactPage from "./contact/page";
import Aboutpage from "./about/page";
import Businesspage from "./busniess/page";
export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between">
      <HomePage />
      {/* <ContactPage />
      <Aboutpage />
      <Businesspage/> */}
    </main>
  );
}
