"use client";

import React from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ReactStars from "react-rating-stars-component";
import client1 from "../../image/c-1.png";
import client2 from "../../image/c-2.png";
import client3 from "../../image/c-3.png";
import client4 from "../../image/c-4.png";
import client5 from "../../image/c-5.png";
import client6 from "../../image/c-6.png";

import Image from "next/image";

const Testimonials = () => {
  const clients = [
    {
      id: 1,
      desc: "Lorem ipsum dolor sit amet, consect adipiscing elit. Pellentesque dignissim nisi a odio laoreet luctus. Ut sed diam, quis bibendum ex",
      pic: client1,
      name: "Musharof Chowdhury",
      rating:5,
    },

    {
      id: 2,
      desc: "Lorem ipsum dolor sit amet, consect adipiscing elit. Pellentesque dignissim nisi a odio laoreet luctus. Ut sed diam, quis bibendum ex",
      pic: client2,
      name: "Mrs Devid Miller",
      rating: 5,
    },

    {
      id: 3,
      desc: "Lorem ipsum dolor sit amet, consect adipiscing elit. Pellentesque dignissim nisi a odio laoreet luctus. Ut sed diam, quis bibendum ex",
      pic: client3,
      name: "Carlos",
      rating:4,
    },
    {
      id: 4,
      desc: "Lorem ipsum dolor sit amet, consect adipiscing elit. Pellentesque dignissim nisi a odio laoreet luctus. Ut sed diam, quis bibendum ex",
      pic: client4,
      name: "Anaa luis",
      rating: 5,
    },
    {
      id: 5,
      desc: "Lorem ipsum dolor sit amet, consect adipiscing elit. Pellentesque dignissim nisi a odio laoreet luctus. Ut sed diam, quis bibendum ex",
      pic: client5,
      name: "Ben duck",
      rating:5,
    },
    {
      id: 6,
      desc: "Lorem ipsum dolor sit amet, consect adipiscing elit. Pellentesque dignissim nisi a odio laoreet luctus. Ut sed diam, quis bibendum ex",
      pic: client6,
      name: "Taylor",
      rating: 4,
    },
  ];
  const settings = {
    dots: false,
    autoplay: true,  
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <>
      <div className="Testimonials container mx-auto my-8">
        <h1 className="text-center font-bold italic text-xl md:text-5xl mb-6">Testimonials.</h1>
        <div className="grid grid-cols-1 md:grid-cols-2">
          <div className="G-1 p-4">
            <h1 className="text-center font-semibold text-4xl italic">What Client say</h1>
            <p className="text-center text-lg p-4 text-gray-500">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
              convallis tortor eros. Donec vitae tortor lacus. Phasellus aliquam
              ante in maximus.
            </p>
          </div>
          <div className="G-2">
            <Slider {...settings}>
              {clients.map((client) => (
                <div key={client.id} className="border-2 p-4 rounded-lg">
                  <div className="flex items-center gap-4">
                    <Image
                      src={client.pic}
                      alt={client.name}
                      width={100}
                      height={100}
                      className="rounded-full"
                    />
                    <div className="">
                    <h4 className="font-semibold text-xl">{client.name}</h4>
                    <ReactStars
                      count={5}
                      value={client.rating}
                      size={24}
                      activeColor="#FFD700"
                      edit={false}
                    />
                    </div>
                  </div>
                  <p className="text-center w-96 max-w-full mx-auto text-base text-gray-500">
                    {client.desc}
                  </p>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </>
  );
};

export default Testimonials;
