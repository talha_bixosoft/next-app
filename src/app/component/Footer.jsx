import React from "react";
import logo from "../../image/logo.svg";
import Image from "next/image";

const FooterSection = () => {
  const footerMap = [
    {
      id: 1,
      head: "Home",
      arr: ["Product", "Pricing", "Business", "Enterprise"],
    },
    {
      id: 2,
      head: "About Us",
      arr: ["Company", "Leadership", "Careers", "Diversity"],
    },
    {
      id: 3,
      head: "Resources",
      arr: ["Andy Guide", "Forum", "Support", "App Directory"],
    },
    {
      id: 4,
      head: "Tutorial",
      arr: [
        "10 Leadership Styles",
        "Executive Summary Tips",
        "Prevent Team Burnout",
        "What are OKRs?",
      ],
    },
  ];

  return (
    <>
      <div className="w-full bg-[#f8fafb] mt-10 border-t-2 ">
        <div className="w-full container mx-auto pt-16">
          <div className="grid grid-cols-1 md:grid-cols-2 max-w-7xl px-4 md:px-0">
            <div className="">
              <figure>
                <Image src={logo} width="100%" height="auto" alt="FooterLogo" />
              </figure>
              <p className="text-base font-medium text-gray-500 mt-4 tracking-wide">
                This membership will help you plan and <br />
                execute a variety of projects
              </p>
            </div>
            <div className="flex flex-wrap justify-between items-center">
              {footerMap.map((item, index) => {
                return (
                  <div key={index} className="footer-map">
                    <h4 className="font-semibold text-lg mb-4">{item.head}</h4>
                    <ul className="text-left mb-4">
                      {item.arr.map((arr, arrIndex) => (
                        <li
                          key={arrIndex}
                          className="text-base text-stone-700 pb-2 font-medium hover:text-violet-500 cursor-pointer"
                        >
                          <a href="#"> {arr} </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <div className="ft-two">
        <div className="flex flex-wrap justify-between bg-indigo-400 py-7 px-7 w-full">
          <h3 className="text-lg font-medium text-white">
            © 2025 Appline. All rights reserved
          </h3>
          <div className="flex gap-4 my-4 md:my-0">
            <span>
              <i class="fa-brands fa-facebook text-2xl font-medium text-white opacity-50 hover:opacity-100 cursor-pointer"></i>
            </span>
            <span>
              <i class="fa-brands fa-twitter text-2xl font-medium text-white opacity-50 hover:opacity-100 cursor-pointer"></i>
            </span>
            <span>
              <i class="fa-brands fa-linkedin text-2xl font-medium text-white opacity-50 hover:opacity-100 cursor-pointer"></i>
            </span>
          </div>
          <div className="flex gap-4">
            <h3 className="text-lg font-medium text-white">Privacy Policy</h3>
            <h5 className="text-lg font-medium text-white">
              Terms and conditions
            </h5>
          </div>
        </div>
      </div>
    </>
  );
};
export default FooterSection;
