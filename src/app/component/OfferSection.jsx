"use client";
import React from "react";
import { useState } from "react";

const OfferSection = () => {
  const [isChecked, setIsChecked] = useState(false);

  const handleToggle = () => {
    setIsChecked(!isChecked);
  };

  const offerOneMonthly = [
    {
      id: 1,
      offerName: "Free",
      des: "Lorem Ipsum is simply dummythe Lorem Ipsum is simply dummythe.",
      payment: "$0/month",
      Select: "Choose Plan",
      plans: [
        "60-day chat history",
        "35 GB cloud storage",
        "24/7 Support",
        "Custom Branding Strategy",
      ],
    },
    {
      id: 2,
      offerName: "Unlimited",
      des: "Lorem Ipsum is simply dummythe.",
      payment: "$35/month",
      Select: "Choose Plan",
      plans: [
        "60-day chat history",
        "35 GB cloud storage",
        "24/7 Support",
        "Custom Branding Strategy",
      ],
    },
    {
      id: 3,
      offerName: "Business",
      des: "Lorem Ipsum is simply dummythe.",
      payment: "$60/month",
      Select: "Choose Plan",
      plans: [
        "60-day chat history",
        "35 GB cloud storage",
        "24/7 Support",
        "Custom Branding Strategy",
      ],
    },
  ];

  const offerYearly = [
    {
      id: 1,
      offerName: "Free",
      des: "Lorem Ipsum is simply dummythe Lorem Ipsum is simply dummythe.",
      payment: "$0/year",
      Select: "Choose Plan",
      plans: [
        "60-day chat history",
        "35 GB cloud storage",
        "24/7 Support",
        "Custom Branding Strategy",
      ],
    },
    {
      id: 2,
      offerName: "Unlimited",
      des: "Lorem Ipsum is simply dummythe.",
      payment: "$450/year   ",
      Select: "Choose Plan",
      plans: [
        "60-day chat history",
        "35 GB cloud storage",
        "24/7 Support",
        "Custom Branding Strategy",
      ],
    },
    {
      id: 3,
      offerName: "Business",
      des: "Lorem Ipsum is simply dummythe.",
      payment: "$800/year",
      Select: "Choose Plan",
      plans: [
        "60-day chat history",
        "35 GB cloud storage",
        "24/7 Support",
        "Custom Branding Strategy",
      ],
    },
  ];
  const plansToShow = isChecked ? offerYearly : offerOneMonthly;

  return (
    <>
      <div className="container mx-auto">
        <h2 className="mb-4 text-center text-3xl font-bold text-black dark:text-white sm:text-4xl md:text-[44px] md:leading-tight">
          Choose Your Plans
        </h2>
        <p className="text-base  text-center my-8">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis
          tortor eros. Donec vitae <br />
          tortor lacus. Phasellus aliquam ante in maximus.
        </p>
        <div className="text-center">
          <label htmlFor="togglePlan" className="inline-flex items-center">
            <input
              id="togglePlan"
              className="sr-only"
              type="checkbox"
              name="togglePlan"
              checked={isChecked}
              onChange={handleToggle}
            />
            <span className="monthly text-base font-semibold text-black dark:text-white">
              Monthly
            </span>
            <span
              className={`mx-5 flex h-[34px] w-[60px] cursor-pointer items-center rounded-full p-[3px] transition-colors duration-300 ${
                isChecked ? "bg-violet-500" : "bg-slate-300"
              }`}
            >
              <span
                className={`block h-7 w-7 rounded-full bg-white transition-transform duration-300 transform ${
                  isChecked ? "translate-x-6" : "translate-x-0"
                }`}
              ></span>
            </span>
            <span className="yearly text-base font-semibold text-black dark:text-white">
              Yearly
            </span>
          </label>
        </div>

        <div className="business-plan grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 max-w-5xl mx-auto gap-4 relative h-auto overflow-hidden md:overflow-visible ">
          {plansToShow.map((item, index) => (
            <div
              key={index}
              className="bg-slate-300 my-8 rounded-lg px-4 py-4 text-center flex flex-col justify-between h-auto relative  transition-transform duration-300 ease-in-out hover:scale-105 cursor-pointer"
            >
              <h3 className="font-semibold text-xl">{item.offerName}</h3>
              <p className="text-stone-500 text-base line-clamp-2 mb-4">
                {item.des}
              </p>
              <h5 className="text-xl font-medium mb-4">{item.payment}</h5>
              <hr></hr>
              <ul className="text-center mb-4">
                {item.plans.map((plan, planIndex) => (
                  <li
                    key={planIndex}
                    className="text-base text-stone-700 pb-2 font-medium"
                  >
                    {plan}
                  </li>
                ))}
              </ul>
              <button className="bg-violet-500 py-2 text-lg font-medium text-white rounded-lg hover:bg-black">
                {item.Select}
              </button>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default OfferSection;
