"use client"
import { useEffect, useRef } from 'react';
import Typed from 'typed.js';

const TypingAnimation = () => {
  const el = useRef(null);
  const typed = useRef(null);

  useEffect(() => {
    const options = {
      strings: ["I,m Muhammad Talha.", "Frontend Developer."],
      typeSpeed: 40,
      backSpeed: 40,
      loop: true,
    };

    typed.current = new Typed(el.current, options);

    return () => {
      typed.current.destroy();
    };
  }, []);

  return (
    <div className="inline-block bg-gradient-1 bg-clip-text text-transparent text-2xl font-semibold">
      <span style={{ whiteSpace: 'pre' }} ref={el} />
    </div>
  );
};

export default TypingAnimation;
