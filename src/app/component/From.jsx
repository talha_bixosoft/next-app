import React from "react";
import Link from 'next/link';


const From = () => {
  return (
    <>
      <div className="form my-8">
        <h1 className="text-center text-xl md:text-4xl  font-semibold">
        Lets Stay Connected
        </h1>
        <p className="text-center text-base text-gray-500 my-2">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis
          tortor eros. Donec vitae <br />
          tortor lacus. Phasellus aliquam ante in maximus
        </p>
      </div>
      <div class=" mx-auto w-full max-w-[925px] rounded-lg bg-slate-300 px-8 py-10  sm:px-5">
  <form>
    <div class=" flex flex-wrap">
      <div class="w-full px-[10px] md:w-1/2">
        <div class="mb-8">
          <input 
            id="name" 
            placeholder="Enter your name" 
            class="w-full rounded border-2 border-stroke bg-white px-[30px] py-4 text-base  outline-none focus:border-sky-500" 
            type="text" 
            name="name" 
          />
        </div>
      </div>
      <div class="w-full px-[10px] md:w-1/2">
        <div class="mb-8">
          <input 
            id="company" 
            placeholder="Company (optional)" 
            class="w-full rounded border-2 border-stroke bg-white px-[30px] py-4 text-base outline-none focus:border-sky-500" 
            type="text" 
            name="company" 
          />
        </div>
      </div>
      <div class="w-full px-[10px] md:w-1/2">
        <div class="mb-8">
          <input 
            id="email" 
            placeholder="Enter your email" 
            class="w-full rounded border-2 border-stroke bg-white px-[30px] py-4 text-base outline-none focus:border-sky-500" 
            type="email" 
            name="email" 
          />
        </div>
      </div>
      <div class="w-full px-[10px] md:w-1/2">
        <div class="mb-8">
          <input 
            id="phone" 
            placeholder="Enter your phone number" 
            class="w-full rounded border-2 border-stroke bg-white px-[30px] py-4 text-base outline-none focus:border-sky-500" 
            type="text" 
            name="phone" 
          />
        </div>
      </div>
      <div class="w-full px-[10px]">
        <div class="mb-8">
          <textarea 
            rows="6" 
            name="message" 
            id="message" 
            placeholder="Tell us about yourself" 
            class="w-full rounded border-2 border-stroke bg-white px-[30px] py-4 text-base  outline-none focus:border-sky-500 resize-none"
          ></textarea>
        </div>
      </div>
      <div class="w-full px-[22px]">
        <div class="text-center">
          <p class="mb-5 text-center text-base">
            By clicking the contact us button, you agree to our terms and policy.
          </p>
          <button 
            type="submit" 
            class="inline-block rounded-md bg-violet-500 px-11 py-[14px] text-base font-medium text-white hover:bg-opacity-80"
          >
           <Link href="/contact">Contact Us</Link> 
          </button>
        </div>
      </div>
    </div>
  </form>
</div>

    </>
  );
};
export default From;
