import Image from "next/image";
import React from "react";
import bannerpic from "../../image/hero-light.webp";
import TypingAnimation from "./Retyping";

const BannerSection = () => {
  return (
    <>
      <div className="container mx-auto">
        <div className="grid grid-cols-1 md:grid-cols-2">
          <div className="intro p-12">
            <span class="mb-5 text-lg font-medium leading-tight text-black dark:text-white sm:text-[22px] xl:text-[22px]">
              Crafted for App, Software and SaaS Sites
            </span>
            <h1 class="mb-6 text-3xl font-bold leading-tight text-black dark:text-white sm:text-[40px] md:text-[50px] lg:text-[42px] xl:text-[50px]">
              Next.js Template and SaaS
              <span class="inline bg-gradient-1 bg-clip-text text-transparent">
                {" "}
                Starter Kit{" "}
              </span>
              Site.
            </h1>
            <p class="mb-5 max-w-[475px] text-base leading-relaxed text-body">
              Website template and starter kit crafted to build fully functional
              mobile app landing pages and software websites.
            </p>
            <TypingAnimation/>
            <div class="flex flex-wrap items-center">
              <a
                class="mb-6 mr-6 inline-flex h-[60px] items-center rounded-lg bg-black px-[30px] py-[14px] text-white hover:bg-opacity-90 dark:bg-white dark:text-black dark:hover:bg-opacity-90"
                href="#"
              >
                <span class="mr-[18px] border-r border-stroke border-opacity-40 pr-[18px] leading-relaxed dark:border-[#BDBDBD]">
                  Download Now
                </span>
                <span>
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g clip-path="url(#clip0_3_3641)">
                      <path
                        d="M11.624 7.2221C10.748 7.2221 9.392 6.2261 7.964 6.2621C6.08 6.2861 4.352 7.3541 3.38 9.0461C1.424 12.4421 2.876 17.4581 4.784 20.2181C5.72 21.5621 6.824 23.0741 8.288 23.0261C9.692 22.9661 10.22 22.1141 11.924 22.1141C13.616 22.1141 14.096 23.0261 15.584 22.9901C17.096 22.9661 18.056 21.6221 18.98 20.2661C20.048 18.7061 20.492 17.1941 20.516 17.1101C20.48 17.0981 17.576 15.9821 17.54 12.6221C17.516 9.8141 19.832 8.4701 19.94 8.4101C18.62 6.4781 16.592 6.2621 15.884 6.2141C14.036 6.0701 12.488 7.2221 11.624 7.2221ZM14.744 4.3901C15.524 3.4541 16.04 2.1461 15.896 0.850098C14.78 0.898098 13.436 1.5941 12.632 2.5301C11.912 3.3581 11.288 4.6901 11.456 5.9621C12.692 6.0581 13.964 5.3261 14.744 4.3901Z"
                        fill="currentColor"
                      ></path>
                    </g>
                    
                    <defs >
                      <clipPath id="clip0_3_3641">
                        <rect width="24" height="24" fill="white"></rect>
                      </clipPath>
                    </defs>
                  </svg>
                </span>
              </a>
              <a
                class="glightbox mb-6 inline-flex items-center py-4 text-black hover:text-primary dark:text-white dark:hover:text-primary"
                href="#"
              >
                <span className="animate__animated animate__bounce animate__slow animate__infinite mr-[22px] flex h-[60px] w-[60px] items-center justify-center rounded-full border-2 border-current">
                  <svg
                    width="14"
                    height="16"
                    viewBox="0 0 14 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13.5 7.06367C14.1667 7.44857 14.1667 8.41082 13.5 8.79572L1.5 15.7239C0.833334 16.1088 -3.3649e-08 15.6277 0 14.8579L6.05683e-07 1.00149C6.39332e-07 0.231693 0.833334 -0.249434 1.5 0.135466L13.5 7.06367Z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </span>
                <span class="text-base font-medium">
                  <span class="block text-sm"> Watch Demo </span>See how it
                  works
                </span>
              </a>
            </div>
          </div>
          <div className="mx-auto">
          <div className="bg-gradient-1 rounded-full w-[350px] h-[350px] lg:w-[450px] lg:h-[450px] flex justify-center items-center">
            <div
              className="w-[200px] lg:[250px] h-[350px]">
                <Image src={bannerpic} width="100%" height="100%" className="max-w-full"/>
            </div>
          </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BannerSection;
