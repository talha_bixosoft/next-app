import React from 'react'
import Navbar from '../component/Navbar';
import FooterSection from '../component/Footer';
import CardSection from '../component/Card';

 const 
 Aboutpage = () => {
  return (
    <>
    <Navbar/>
    <CardSection/>
    <FooterSection/>
    
    </>
  )
};
export default Aboutpage;