import React from "react";
import Navbar from "@/app/component/Navbar";
import OfferSection from "@/app/component/OfferSection";
import FooterSection from "@/app/component/Footer";

const Businesspage = () => {
  return (
    <div>
      <Navbar />
      <OfferSection />
      <FooterSection />
    </div>
  );
};
export default Businesspage;
