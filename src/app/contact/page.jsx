import React from "react";
import Navbar from "../component/Navbar";
import FooterSection from "../component/Footer";
import mypic from "../../image/my-pic.jpg";
import Image from "next/image";

const ContactPage = () => {
  return (
    <>
      <Navbar />
      <div className="flex flex-wrap items-center">
        <div className="">
          <figure className="p-8">
            <Image
              src={mypic}
              width={300}
              height="auto"
              alt="talha"
              className="max-w-full rounded-xl border-4 border-violet-500  animate__animated animate__backInDown animate__slower animate__infinite"
            />
          </figure>
        </div>
        <div className="my-self">
          <div className="py-8">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              className="size-12 rotate-45 text-indigo-500 animate__animated animate__tada animate__fast animate__infinite"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M10.05 4.575a1.575 1.575 0 1 0-3.15 0v3m3.15-3v-1.5a1.575 1.575 0 0 1 3.15 0v1.5m-3.15 0 .075 5.925m3.075.75V4.575m0 0a1.575 1.575 0 0 1 3.15 0V15M6.9 7.575a1.575 1.575 0 1 0-3.15 0v8.175a6.75 6.75 0 0 0 6.75 6.75h2.018a5.25 5.25 0 0 0 3.712-1.538l1.732-1.732a5.25 5.25 0 0 0 1.538-3.712l.003-2.024a.668.668 0 0 1 .198-.471 1.575 1.575 0 1 0-2.228-2.228 3.818 3.818 0 0 0-1.12 2.687M6.9 7.575V12m6.27 4.318A4.49 4.49 0 0 1 16.35 15m.002 0h-.002"
              />
            </svg>

            <h1 className="text-xl md:text-5xl mt-4">
              M.Talha Mehmood
            </h1>
            <p className="text-xl md:text-3xl my-4">
            khalidtalha485@gmail.com
            </p>
            <div className="flex gap-8 my-4 md:my-0">
              <span>
                <a
                  href="https://www.facebook.com/huzaifayaman.yaman.3?mibextid=ZbWKwL"
                  target="blank"
                >
                  <i class="fa-brands fa-facebook text-xl md:text-4xl font-medium text-blue-500 opacity-50 hover:opacity-100 cursor-pointer"></i>
                </a>
              </span>
              <span>
                <a
                  href="https://www.instagram.com/huzaifayamanyaman?igsh=MjBvMzVnMzhobjFp"
                  target="blank"
                >
                  <i class="fa-brands fa-instagram text-xl md:text-4xl  font-medium text-blue-500 opacity-50 hover:opacity-100 cursor-pointer"></i>
                </a>
              </span>
              <span>
                <a
                  href="https://www.linkedin.com/in/talha-khalid-1aa4b4267?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=android_app"
                  target="blank"
                >
                  <i class="fa-brands fa-linkedin text-xl md:text-4xl  font-medium text-blue-500 opacity-50 hover:opacity-100 cursor-pointer"></i>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
      <FooterSection />
    </>
  );
};
export default ContactPage;
