'use client'
import React, { useEffect, useState } from 'react';
import Navbar from '../component/Navbar';
import BannerSection from '../component/BannerSection';
import CardSection from '../component/Card';
import OfferSection from '../component/OfferSection';
import Testimonials from '../component/Testimonials';
import From from '../component/From';
import FooterSection from '../component/Footer';
import '../../app/style.css';


const HomePage = () =>{

  const [isVisible, setIsVisible] = useState(false);

  const handleScroll = () => {
    if (window.scrollY > 100) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };


useEffect(() => {
  window.addEventListener('scroll', handleScroll);
  return () => {
    window.removeEventListener('scroll', handleScroll);
  };
}, []);


  return(
    <>
    <Navbar/>
    <BannerSection/>
    <CardSection/>
    <OfferSection/>
    <Testimonials/>
    <From/>
    <FooterSection/>

    <div
      className={`${isVisible ? 'backToTop': ''}`}
      onClick={scrollToTop}
    >
      ↑
    </div>
    </>
  )
};


export default HomePage;
